#!/usr/bin/env nix-shell
#! nix-shell -i bash -p hugo

hugo
rsync -r ./public/ root@backend1.banditlair.com:/nix/var/data/website-marie
